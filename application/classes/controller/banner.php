<?php 


/**
* 
*/
class Controller_Banner extends Controller
{

	public function action_slider()
	{
		$slider = new View('banner/slider');
		$slider->slides = Service_Banner::get_list($this->application_context->get_actual_language_id());
		$this->request->response = $slider->render();	
	}	
}
<section class="news wrapper row">
	<h3 style="font-size: 23px; font-weight: 600;">Aktuality</h3>
    	<div class="row-same-height">

	{foreach from=$items item=article key=key name=articles}
		<div class="newsItem col-md-6 col-md-height">
			<h2><a href="{$url_base}{$article.nazev_seo}">{$article.nazev}</a></h2>
			<p>{$article.popis|truncate:120:"..."|strip_tags:false}</p>
			<a href="{$url_base}{$article.nazev_seo}">
				<span>
					<strong style="font-size: 15px;">{translate str="více"}</strong>
				</span>
			</a>
		</div>
	{/foreach}

    	</div>
</section>


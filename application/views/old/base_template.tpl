{*
CMS system Hana ver. 2.6 (C) Pavel Herink 2012
zakladni spolecna sablona layoutu stranek

automaticky generovane promenne spolecne pro vsechny sablony:
-------------------------------------------------------------
    $url_base      - zakladni url
    $tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
    $url_actual    - autualni url
$url_homepage  - cesta k homepage
    $media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
$controller
$controller_action
$language_code - kod aktualniho jazyku
$is_indexpage  - priznak, zda jsme na indexove strance

    $web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
    $web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
-------------------------------------------------------------

doplnujici custom Smarty funkce
-------------------------------------------------------------
                                    {translate str="nazev"}                                      - prelozeni retezce
                    {static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
    {widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
        {hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
{hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
{$product.cena|currency:$language_code}

Promenne do base_template:
-------------------------------------------------------------
{$page_description}
{$page_keywords}
{$page_name} - {$page_title}
{$main_content}
{include file="`$tpl_dir`dg_footer.tpl"}
{if !empty($web_owner.ga_script)}
    {$web_owner.ga_script}
    {/if}


*}
<!DOCTYPE html>
<html>
<head lang="{$language_code}">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{$media_path}css/main.css"/>
    <script src="{$media_path}js/jquery.js" type="application/javascript"></script>
    <script src="{$media_path}js/jqueryUI.js" type="application/javascript"></script>
    <script src="{$media_path}js/bxslider/plugins/jquery.easing.1.3.js" type="application/javascript"></script>
    <script src="{$media_path}js/bxslider/jquery.bxslider.min.js" type="application/javascript"></script>
    <script src="{$media_path}js/main.js" type="application/javascript"></script>
    <title>{$page_name} - {$page_title}</title>
</head>
<body>
<header class="wrapper">
<div>
    <div class="row">
        <div class="col-xs-9" id="logo">
                <a href=""><img src="{$media_path}img/layout/logo.png" class="img-responsive" alt="{translate str=$web_owner.default_description}"/></a>

                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <h2>{translate str="půjčovna"}<br/>{translate str="tryskacího zařízení"}</h2>
                    </div>
                </div>
            </div>
            {*<div class="col-xs-3 pullRight">
                {widget controller="site" action="languagebox"}
            </div>*}
        </div>
    </div>
</header>
    {widget controller="navigation" action="main"}
    {$main_content}
    {widget controller="article" action="widget"}
    <footer>
        <div class="footerBottom">
            <div class="wrapper">
                <strong>{$web_owner.copyright}</strong>
                <div class="row">
                    <div class="col-sm-9">realizace:
                        <a href="http://www.dgstudio.cz/" title="tvorba www stránek">tvorba www stránek</a> |
                        <a href="http://www.virtualni-prohlidky.eu/" title="virtuální prohlídky">virtuální prohlídky</a> |
                        <a href="http://validator.w3.org/check/referer" title="HTML validátor">HTML5</a> |
                        <a href="http://jigsaw.w3.org/css-validator/" title="CSS validator">CSS 3</a>
                    </div>
                    <div class="col-sm-3">
                        <a href="http://www.dgstudio.cz/" title="DG Studio" id="dgLogo"><div></div></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
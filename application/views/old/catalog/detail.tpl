<section>
    <article id="productDetail">
	 <section class="wrapper">
    <h1>{$item.nazev}</h1>
    <div class="row">
    {widget controller="navigation" action="category"}
        <div class="col-md-9">
            <section class="row productInfo">
                {if not empty($item.photo_detail)}
                    <div class="col-lg-6">
                        <div class="productGallery">
                            <img src="{$item.photo_detail}" class="img-responsive" alt=""/>
                        </div>
                    </div>
                {/if}
                <div class="col-lg-{if not empty($item.photo_detail)}6{else}12{/if}">
                    <h2>{$item.nazev}</h2>
                    <p>{$item.popis}</p>
                </div>
            </section>
            {widget controller="contact" action="show"}
        </div>
    </div>
	</section>
    </article>
</section>
<section class="tabs">
    <div class="wrapper row">
        <div class="row-same-height">
            {foreach from=$categories item=category key=key name=catalog}
                <div class="col-sm-4 col-sm-height">
                {if count($category.products)>0}
                    {foreach from=$category.products item=product key=key name=products}
                        {if $smarty.foreach.products.first}
                                <a href="{$url_base}{$product.nazev_seo}">
                                    <div class="tabImg">
                                        <img src="{$category.photo_detail}" class="img-responsive" alt=""/>
                                        <span>
                                            <strong>{$category.nazev}</strong>
                                        </span>
                                    </div>
                                </a>
                                <ul>

                        {/if}
                        {if not $product.strong}<li><a href="{$url_base}{$product.nazev_seo}">{$product.nazev}</a></li>{/if}
                    {/foreach}
                                </ul>
                {/if}

                {if not empty($category.buttons)}
                    <div class="tabButtons">
                        {foreach from=$category.buttons item=button key=key name=buttonsUnder}
                            <a href="{$button.nazev_seo}">
                                <span class="makeMeButton {if $smarty.foreach.buttonsUnder.first}dark{/if}">{$button.nazev}</span>
                            </a>
                        {/foreach}
                    </div>
                {/if}
                </div>
            {/foreach}
        </div>
    </div>
</section>
<section id="contactArea">
    <p><b>{translate str="Zaujaly Vás informace na této stránce? Potřebujete znát více informací? Zašlete nám Váš dotaz."}</b></p>
    <form action="#" id="contactForm" class="row" method="post">
        <div class="col-md-5">
            <label for="contactform[jmeno]" class="required">{translate str='Jméno'}</label>
            <br/>
            <input type="text" name="contactform[jmeno]" id="name" required=""/>
            <br/>
            <label for="contactform[email]" class="required">{translate str='Email'}</label>
            <br/>
            <input type="email" name="contactform[email]" id="email" required=""/>
            <br/>
            <label for="contactform[telefon]" class="required">{translate str='Telefon'}</label>
            <br/>
            <input type="tel" name="contactform[telefon]" id="tel" required=""/>
        </div>
        <div class="col-md-7">
            <label for="contactform[zprava]" class="required">{translate str='Text'}</label>
            <br/>
            <textarea name="contactform[zprava]" id="text" required=""></textarea>
        </div>
        {hana_secured_post action="send" module="contact"}
    </form>

    <button type="submit" class="makeMeButtonOrange" form="contactForm">{translate str='Poptat'}</button>
    <div class="cleaner"></div>
</section>
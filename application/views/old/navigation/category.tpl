
<nav class="col-md-3" id="sideNav">
                    <div class="sideNavToggle">
                        <span></span>
                    </div>
                    <div class="sideNavItems">
                    {foreach from=$links item=category key=key name=Underlinks}
                        <section>
                            <h2>{$category.nazev}</h2>
                            <ul>
                            {foreach from=$category.products item=product key=key name=name}
                            	 <li {*class="active"*}>
                                    {if $product.strong}<strong>{/if}
                                        <a href="{$url_base}{$product.nazev_seo}">{$product.nazev}</a>
                                    {if $product.strong}</strong> {/if}    
                                </li>
                            {/foreach}
                            </ul>
                        </section>
                    {/foreach}
                    </div>
                </nav>
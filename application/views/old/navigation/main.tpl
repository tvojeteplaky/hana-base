<nav id="mainNav" class="wrapper">
	<a href="#" class="navToggle"><span></span></a>
	<div class="nav">
		{foreach from=$links item=link key=key name=menu}
		<a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a>
		{/foreach}
		<div class="navButtons">
			<a href="http://www.wista.cz/">
				<span class="makeMeButton">wista.cz</span>
			</a>
			<a href="http://www.tryskani-lakovani-metalizace.cz/">
				<span class="makeMeButtonOrange">e-shop</span>
			</a>
		</div>
	</div>
</nav>
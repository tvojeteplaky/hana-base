<section>
        <article {if not empty($item.nav_class)}id="{$item.nav_class}"{/if}>
            <div class="wrapper">
            	<h1>{$item.nadpis}</h1>
                    <div class="row">
                        <div class="col-sm-{if not empty($item.photo)}6 pullLeft{else}12{/if}">
                        {$item.popis}
                        </div>
                        {if not empty($item.photo)}
                        <div class="col-sm-6 pullRight">
                            <img src="{$item.photo}" class="img-responsive" alt="{$item.nazev}"/>
                        </div>
                        {/if}
                    </div>
            </div>
    </article>
</section>
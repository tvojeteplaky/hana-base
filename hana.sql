-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Sob 18. dub 2015, 11:12
-- Verze serveru: 5.5.41-0ubuntu0.14.04.1
-- Verze PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `hana`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_preferences`
--

CREATE TABLE IF NOT EXISTS `admin_preferences` (
  `id` int(10) unsigned NOT NULL,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_settings`
--

CREATE TABLE IF NOT EXISTS `admin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email_podpora_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `logo_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `kohana_debug_mode` tinyint(4) NOT NULL DEFAULT '0',
  `smarty_console` tinyint(4) NOT NULL DEFAULT '0',
  `shutdown` tinyint(4) NOT NULL DEFAULT '0',
  `disable_login` int(11) NOT NULL DEFAULT '0',
  `interni_poznamka` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `nazev_dodavatele`, `email_podpora_dodavatele`, `logo_dodavatele`, `kohana_debug_mode`, `smarty_console`, `shutdown`, `disable_login`, `interni_poznamka`) VALUES
(1, 'jkhjkj', '', NULL, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure`
--

CREATE TABLE IF NOT EXISTS `admin_structure` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `submodule_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_controller` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin_menu_section_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `global_access_level` tinyint(4) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=47 ;

--
-- Vypisuji data pro tabulku `admin_structure`
--

INSERT INTO `admin_structure` (`id`, `module_code`, `submodule_code`, `module_controller`, `admin_menu_section_id`, `poradi`, `parent_id`, `zobrazit`, `global_access_level`, `available_languages`) VALUES
(1, 'environment', 'module', 'default', 1, 1, 0, 1, 0, 5),
(2, 'environment', 'setting', 'edit/1/1', 3, 15, 1, 1, 0, 5),
(3, 'environment', 'samodules', 'list', 3, 4, 1, 1, 0, 5),
(4, 'environment', 'salanguages', 'list', 3, 14, 1, 0, 0, 1),
(5, 'environment', 'routes', 'list', 3, 6, 1, 1, 3, 1),
(6, 'environment', 'mainsetup', 'edit/1/1', 3, 1, 1, 1, 3, 1),
(7, 'environment', 'inmodules', 'list', 3, 2, 1, 0, 3, 1),
(8, 'environment', 'registry', 'list', 3, 5, 1, 1, 3, 1),
(9, 'page', 'item', 'list', 1, 2, 0, 1, 0, 1),
(10, 'page', 'item', 'list', 3, 1, 9, 1, 0, 1),
(11, 'page', 'unrelated', 'list', 3, 2, 9, 1, 0, 1),
(12, 'static', 'item', 'list', 3, 3, 9, 1, 0, 1),
(13, 'user', 'item', 'list', 1, 9, 0, 1, 0, 1),
(14, 'article', 'item', 'list', 1, 3, 0, 1, 0, 1),
(15, 'product', 'item', 'list', 1, 5, 0, 0, 0, 1),
(16, 'product', 'item', 'list', 2, 2, 15, 1, 0, 1),
(17, 'product', 'category', 'list', 2, 3, 15, 1, 0, 1),
(18, 'product', 'manufacturer', 'list', 2, 5, 15, 0, 0, 1),
(19, 'product', 'order', 'list', 2, 1, 15, 1, 0, 1),
(20, 'product', 'shipping', 'list', 2, 6, 15, 1, 0, 1),
(21, 'product', 'payment', 'list', 2, 7, 15, 1, 0, 1),
(22, 'product', 'price', 'list', 2, 8, 15, 1, 0, 1),
(23, 'product', 'tax', 'list', 2, 9, 15, 1, 0, 1),
(24, 'page', 'system', 'list', 3, 4, 9, 1, 3, 1),
(25, 'email', 'queue', 'list', 1, 10, 0, 1, 0, 1),
(26, 'email', 'queue', 'list', 3, 1, 25, 1, 0, 1),
(27, 'email', 'type', 'list', 3, 2, 25, 1, 0, 1),
(28, 'email', 'receiver', 'list', 3, 3, 25, 1, 0, 1),
(29, 'email', 'smtp', 'edit/1/1', 3, 4, 25, 1, 0, 1),
(30, 'product', 'shopper', 'list', 2, 4, 15, 1, 0, 1),
(31, 'product', 'orderstates', 'list', 2, 10, 15, 1, 0, 1),
(32, 'product', 'eshopsettings', 'edit/1/1', 2, 11, 15, 1, 0, 1),
(33, 'product', 'voucher', 'list', 2, 12, 15, 1, 0, 1),
(34, 'environment', 'langstrings', 'list', 3, 16, 1, 1, 1, 1),
(35, 'newsletter', 'item', 'list', 1, 15, 0, 0, 0, 1),
(36, 'newsletter', 'item', 'list', 3, 1, 35, 1, 0, 1),
(37, 'newsletter', 'recipient', 'list', 3, 2, 35, 1, 0, 1),
(38, 'comments', 'item', 'list', 1, 11, 0, 0, 0, 1),
(39, 'banner', 'item', 'list', 1, 5, 0, 0, 0, 1),
(40, 'giftbox', 'item', 'list', 2, 13, 15, 0, 0, 1),
(41, 'catalog', 'item', 'list', 1, 4, 0, 1, 0, 1),
(42, 'catalog', 'category', 'list', 2, 1, 41, 0, 0, 1),
(43, 'faq', 'item', 'list', 1, 6, 0, 1, 0, 1),
(44, 'gallery', 'item', 'list', 1, 8, 0, 1, 0, 1),
(45, 'download', 'item', 'list', 1, 7, 0, 1, 0, 1),
(46, 'download', 'category', 'list', 2, 1, 45, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure_data`
--

CREATE TABLE IF NOT EXISTS `admin_structure_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_structure_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `admin_module_id` (`admin_structure_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=50 ;

--
-- Vypisuji data pro tabulku `admin_structure_data`
--

INSERT INTO `admin_structure_data` (`id`, `admin_structure_id`, `language_id`, `nazev`, `nadpis`, `title`, `description`, `keywords`, `popis`) VALUES
(1, 1, 1, 'Základní', 'Základní', 'Základní', '', '', NULL),
(6, 6, 1, 'Nastavení projektu', 'Základní nastavení', 'Základní nastavení', '', '', NULL),
(3, 3, 1, 'Struktura administrace', 'Struktura administrace', 'Struktura administrace', '', '', NULL),
(7, 7, 1, 'Instalátor modulů', 'Instalátor modulů', 'Instalátor modulů', '', '', NULL),
(8, 8, 1, 'Registry modulů', 'Registry modulů', 'Registry modulů', '', '', NULL),
(5, 5, 1, 'Záznamy rout', 'Záznamy rout', 'Záznamy rout', '', '', NULL),
(4, 4, 1, 'Nastavení jazyků', 'Nastavení jazyků', 'Nastavení jazyků', '', '', NULL),
(2, 2, 1, 'Obecné nastavení', 'Obecné nastavení', 'Obecné nastavení', '', '', NULL),
(9, 9, 1, 'Stránky', 'Stránky', 'Stránky', '', '', NULL),
(10, 10, 1, 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', '', '', NULL),
(11, 11, 1, 'Nezařazené stránky', 'Nezařazené stránky', 'Nezařazené stránky', '', '', NULL),
(12, 12, 1, 'Statický obsah', 'Statický obsah', 'Statický obsah', '', '', NULL),
(13, 13, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', '', NULL),
(14, 14, 1, 'Novinky', 'Novinky', 'Novinky', '', NULL, NULL),
(15, 15, 1, 'E-shop', 'E-shop', 'E-shop', '', NULL, NULL),
(16, 16, 1, 'Seznam produktů', 'Seznam produktů', 'Seznam produktů', '', NULL, NULL),
(17, 17, 1, 'Kategorie produktů', 'Kategorie produktů', 'Kategorie produktů', '', NULL, NULL),
(18, 18, 1, 'Výrobci', 'Výrobci', 'Výrobci', '', NULL, NULL),
(19, 19, 1, 'Objednávky', 'Objednávky', 'Objednávky', '', NULL, NULL),
(20, 20, 1, 'Doprava', 'Doprava', 'Doprava', '', NULL, NULL),
(21, 21, 1, 'Platba', 'Platba', 'Platba', '', NULL, NULL),
(22, 22, 1, 'Cenové skupiny', 'Cenové skupiny', 'Cenové skupiny', '', NULL, NULL),
(23, 23, 1, 'DPH', 'DPH', 'DPH', '', NULL, NULL),
(24, 24, 1, 'Systémové stránky', 'Systémové stránky', 'Systémové stránky', '', NULL, NULL),
(25, 25, 1, 'Email', 'Email', 'Email', '', NULL, NULL),
(26, 26, 1, 'Fronta emailů', 'Fronta emailů', 'Fronta emailů', '', NULL, NULL),
(27, 27, 1, 'Typy emailů', 'Typy emailů', 'Typy emailů', '', NULL, NULL),
(28, 28, 1, 'Příjemci emailů', 'Příjemci emailů', 'Příjemci emailů', '', NULL, NULL),
(29, 29, 1, 'Nastavení SMTP', 'Nastavení SMTP', 'Nastavení SMTP', '', NULL, NULL),
(30, 30, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', NULL, NULL),
(31, 31, 1, 'Stavy objednávek', 'Stavy objednávek', 'Stavy objednávek', '', NULL, NULL),
(32, 32, 1, 'Nastavení eshopu', 'Nastavení eshopu', 'Nastavení eshopu', '', NULL, NULL),
(33, 33, 1, 'Slevové kupóny', 'Slevové kupóny', 'Slevové kupóny', '', NULL, NULL),
(34, 34, 1, 'Statické překlady', 'Statické překlady', 'Statické překlady', '', NULL, NULL),
(35, 35, 1, 'Newsletter', 'Newsletter', 'Newsletter', '', NULL, NULL),
(36, 36, 1, 'Newsletter', 'Newsletter', 'Newsletter', '', NULL, NULL),
(37, 37, 1, 'Newsletter - příjemci', 'Newsletter - příjemci', 'Newsletter - příjemci', '', NULL, NULL),
(38, 38, 1, 'Poradna', 'Poradna', 'Poradna', '', NULL, NULL),
(39, 39, 1, 'bannery', 'bannery', 'bannery', '', NULL, NULL),
(40, 40, 1, 'Dárkové balení', 'Dárkové balení', 'Dárkové balení', '', NULL, NULL),
(41, 41, 1, 'Katalog', 'Katalog', 'Katalog', '', NULL, NULL),
(42, 42, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(43, 43, 1, 'Poradna', 'Poradna', 'Poradna', '', NULL, NULL),
(44, 44, 1, 'Galerie', 'Galerie', 'Galerie', '', NULL, NULL),
(45, 45, 1, 'Ke stažení', 'Ke stažení', 'Ke stažení', '', NULL, NULL),
(46, 46, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(47, 1, 4, 'Defaults', 'Defaults', 'Defaults', '', NULL, NULL),
(48, 3, 4, 'Administration structure', 'Samodules', 'Samodules', '', NULL, NULL),
(49, 2, 4, 'Default setting', 'Default setting', 'Default setting', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `poradi` int(11) NOT NULL,
  `article_category_id` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `gallery_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_data`
--

CREATE TABLE IF NOT EXISTS `article_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `article_category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`article_id`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photos`
--

CREATE TABLE IF NOT EXISTS `article_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photo_data`
--

CREATE TABLE IF NOT EXISTS `article_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_photo_id` (`article_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `banner_data`
--

CREATE TABLE IF NOT EXISTS `banner_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `banner_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `text_question` text COLLATE utf8_czech_ci,
  `text_response` text COLLATE utf8_czech_ci,
  `authorized` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`product_id`,`authorized`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `download_category_id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `download_categories`
--

CREATE TABLE IF NOT EXISTS `download_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `poradi` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `download_category_data`
--

CREATE TABLE IF NOT EXISTS `download_category_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `download_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `nadpis` varchar(255) NOT NULL,
  `popis` text,
  `uvodni_popis` text,
  `zobrazit` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `download_data`
--

CREATE TABLE IF NOT EXISTS `download_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `file_src` varchar(255) DEFAULT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue`
--

CREATE TABLE IF NOT EXISTS `email_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_to_email` varchar(255) NOT NULL,
  `queue_to_name` varchar(255) DEFAULT NULL,
  `queue_cc_email` varchar(255) DEFAULT NULL,
  `queue_cc_name` varchar(255) DEFAULT NULL,
  `email_queue_body_id` int(11) NOT NULL,
  `queue_sent` tinyint(4) NOT NULL DEFAULT '0',
  `queue_sent_date` datetime DEFAULT NULL,
  `queue_priority` int(11) NOT NULL DEFAULT '0',
  `queue_date_to_be_send` datetime DEFAULT NULL,
  `queue_create_date` datetime NOT NULL,
  `queue_errors_count` tinyint(4) NOT NULL DEFAULT '0',
  `queue_error` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue_bodies`
--

CREATE TABLE IF NOT EXISTS `email_queue_bodies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_subject` varchar(255) DEFAULT NULL,
  `queue_from_email` varchar(255) NOT NULL,
  `queue_from_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `queue_body` text NOT NULL,
  `queue_attached_file` varchar(255) DEFAULT NULL,
  `queue_newsletter_id` int(11) DEFAULT NULL,
  `queue_shopper_id` bigint(20) DEFAULT NULL,
  `queue_branch_id` bigint(20) DEFAULT NULL,
  `queue_order_id` bigint(20) DEFAULT NULL,
  `queue_user_id` bigint(20) DEFAULT NULL,
  `queue_email_type_id` int(11) DEFAULT NULL,
  `queue_send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`queue_email_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_receivers`
--

CREATE TABLE IF NOT EXISTS `email_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `email_receivers`
--

INSERT INTO `email_receivers` (`id`, `nazev`, `email`, `user_id`) VALUES
(1, 'tom.barborik@dgstudio.cz / developer', 'tom.barborik@dgstudio.cz', 0),
(2, 'Dencop / marketing', 'marketing@dencop.cz', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL,
  `mailer` varchar(16) NOT NULL DEFAULT 'mail',
  `host` varchar(32) NOT NULL DEFAULT 'localhost',
  `port` int(11) NOT NULL DEFAULT '25',
  `SMTPSecure` varchar(32) DEFAULT NULL,
  `SMTPAuth` tinyint(4) NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `SMTPDebug` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_settings`
--

INSERT INTO `email_settings` (`id`, `mailer`, `host`, `port`, `SMTPSecure`, `SMTPAuth`, `username`, `password`, `SMTPDebug`) VALUES
(1, 'smtp', 'mail.dghost.cz', 25, '', 1, 'smtp@dghost.cz', 'smtpmageror', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types`
--

CREATE TABLE IF NOT EXISTS `email_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `from_nazev` varchar(255) DEFAULT '',
  `from_email` varchar(255) DEFAULT '',
  `use_email_queue` tinyint(4) NOT NULL DEFAULT '0',
  `send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `email_types`
--

INSERT INTO `email_types` (`id`, `nazev`, `code`, `template`, `subject`, `from_nazev`, `from_email`, `use_email_queue`, `send_by_cron`) VALUES
(1, 'Kontaktní formulář', 'form_contact', '', 'Kontaktní formulář z dencop.cz', '', '', 1, 0),
(2, 'Poptávkový formulář', 'form_demand', '', 'Poptávkový formulář z dencop.cz', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types_receivers`
--

CREATE TABLE IF NOT EXISTS `email_types_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type_id` int(11) NOT NULL,
  `email_receiver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`email_type_id`,`email_receiver_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `email_types_receivers`
--

INSERT INTO `email_types_receivers` (`id`, `email_type_id`, `email_receiver_id`) VALUES
(3, 1, 2),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `eshop_settings`
--

CREATE TABLE IF NOT EXISTS `eshop_settings` (
  `id` int(11) NOT NULL,
  `billing_data_nazev` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ulice` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_mesto` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_psc` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_dic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_telefon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_fax` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_banka` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_iban` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_cislo_uctu` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_konst_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_spec_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_swift` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `present_enabled` tinyint(4) NOT NULL,
  `present_price_threshold` decimal(10,0) NOT NULL,
  `billing_data_due_date` int(11) DEFAULT NULL,
  `shipping_free_threshold` decimal(6,0) DEFAULT NULL,
  `first_purchase_discount` decimal(4,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `eshop_settings`
--

INSERT INTO `eshop_settings` (`id`, `billing_data_nazev`, `billing_data_email`, `billing_data_ulice`, `billing_data_mesto`, `billing_data_psc`, `billing_data_ic`, `billing_data_dic`, `billing_data_telefon`, `billing_data_fax`, `billing_data_banka`, `billing_data_iban`, `billing_data_cislo_uctu`, `billing_data_konst_s`, `billing_data_spec_s`, `billing_data_swift`, `present_enabled`, `present_price_threshold`, `billing_data_due_date`, `shipping_free_threshold`, `first_purchase_discount`) VALUES
(1, 'dgstudio.cz, s.r.o.', 'info@ledmarket.cz', 'U Zimního stadionu 1095', 'Zlín', '760 01', '03127281', 'CZ03127281', '725 809 909', '', 'FIO', '', '2300618928/2010', '', '', '', 0, 400, 14, 2500, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `poradi` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `faq_data`
--

CREATE TABLE IF NOT EXISTS `faq_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_id` tinyint(1) NOT NULL,
  `otazka` varchar(255) NOT NULL,
  `odpoved` text NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `poradi` int(11) NOT NULL,
  `realizace` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `gallery_data`
--

CREATE TABLE IF NOT EXISTS `gallery_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `nadpis` varchar(255) DEFAULT NULL,
  `uvodni_popis` text,
  `popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `gallery_photos`
--

CREATE TABLE IF NOT EXISTS `gallery_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `photo_src` varchar(255) NOT NULL,
  `ext` varchar(255) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `gallery_photo_data`
--

CREATE TABLE IF NOT EXISTS `gallery_photo_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `nazev` varchar(255) NOT NULL,
  `popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `gift_boxes`
--

CREATE TABLE IF NOT EXISTS `gift_boxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `invoice_settings`
--

CREATE TABLE IF NOT EXISTS `invoice_settings` (
  `id` int(11) NOT NULL,
  `next_invoice_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_strings`
--

CREATE TABLE IF NOT EXISTS `language_strings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_string_data`
--

CREATE TABLE IF NOT EXISTS `language_string_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `language_string_id` int(11) NOT NULL,
  `string` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_string_id`) USING BTREE,
  KEY `language_id_2` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `photo` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `poradi` int(11) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturer_data`
--

CREATE TABLE IF NOT EXISTS `manufacturer_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `manufacturer_id` (`manufacturer_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `verze` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `poznamka` text COLLATE utf8_czech_ci,
  `poradi` int(11) NOT NULL DEFAULT '0',
  `admin_zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available` tinyint(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `kod` (`kod`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=17 ;

--
-- Vypisuji data pro tabulku `modules`
--

INSERT INTO `modules` (`id`, `kod`, `nazev`, `verze`, `datum`, `autor`, `url`, `email`, `popis`, `poznamka`, `poradi`, `admin_zobrazit`, `available`) VALUES
(1, 'page', 'Stránky', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zobrazující klasický statický obsah ve stránkách.', NULL, 1, 1, 1),
(2, 'link', 'Odkazy', '1', NULL, 'Pavel Herink', NULL, NULL, 'Odkaz na externí stránku.', NULL, 2, 1, 1),
(3, 'article', 'Články', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul článků a novinek.', NULL, 3, 1, 1),
(4, 'contact', 'Kontaktní formulář', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul kontaktního formuláře.', NULL, 4, 1, 1),
(5, 'photo', 'Fotogalerie', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul fotogalerie.', NULL, 5, 1, 1),
(6, 'catalog', 'Katalog', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul produktů.', NULL, 6, 1, 1),
(7, 'shoppingcart', 'Košík', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul objednávky a košíku.', NULL, 7, 0, 1),
(8, 'user', 'Zákazník', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zákaznického účtu.', NULL, 8, 0, 1),
(9, 'order', 'Objednávka', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 9, 0, 1),
(10, 'search', 'Vyhledávání', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 10, 1, 0),
(11, 'newsletter', 'Newsletter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 0, 1),
(12, 'faq', 'Poradna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 1, 1),
(13, 'download', 'Ke stažení', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 1, 1),
(14, 'gallery', 'Galerie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, 1, 1),
(15, 'demand', 'Poptávkový formulář', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 1, 0),
(16, 'sitemap', 'Mapa stránek', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `module_actions`
--

CREATE TABLE IF NOT EXISTS `module_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `kod` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `povoleno` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`,`povoleno`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `generovan` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_data`
--

CREATE TABLE IF NOT EXISTS `newsletter_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`newsletter_id`) USING BTREE,
  KEY `newsletter_id` (`newsletter_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_recipients`
--

CREATE TABLE IF NOT EXISTS `newsletter_recipients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `allowed` tinyint(3) unsigned NOT NULL,
  `shopper_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `shopper_id` (`shopper_id`) USING BTREE,
  KEY `allowed` (`allowed`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_code` char(13) NOT NULL,
  `order_code_invoice` varchar(12) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `order_date_finished` date DEFAULT NULL,
  `order_delivery_date` datetime DEFAULT NULL,
  `order_date_tax` datetime DEFAULT NULL,
  `order_date_payment` datetime DEFAULT NULL,
  `order_payment_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_shipping_id` tinyint(4) NOT NULL,
  `order_const_symbol` varchar(4) NOT NULL DEFAULT '0',
  `order_state_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_price_no_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_no_vat_rate` int(11) NOT NULL DEFAULT '0',
  `order_lower_vat_rate` int(11) NOT NULL DEFAULT '10',
  `order_higher_vat_rate` int(11) NOT NULL DEFAULT '20',
  `order_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_without_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_with_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shipping_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `order_payment_price` decimal(6,2) NOT NULL,
  `order_total` decimal(9,2) NOT NULL,
  `order_voucher_id` int(11) DEFAULT NULL,
  `order_voucher_discount` decimal(10,2) DEFAULT NULL,
  `order_discount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_correction` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_CZK` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_weight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shopper_id` int(11) DEFAULT '0',
  `order_shopper_branch` int(11) DEFAULT '0',
  `order_shopper_name` varchar(255) NOT NULL,
  `order_shopper_code` char(6) DEFAULT NULL,
  `order_shopper_email` varchar(100) NOT NULL,
  `order_shopper_phone` varchar(20) NOT NULL,
  `order_shopper_ic` varchar(64) DEFAULT NULL,
  `order_shopper_dic` varchar(20) DEFAULT NULL,
  `order_shopper_street` varchar(50) DEFAULT NULL,
  `order_shopper_city` varchar(50) DEFAULT NULL,
  `order_shopper_zip` varchar(10) DEFAULT NULL,
  `order_shopper_note` text NOT NULL,
  `order_branch_name` varchar(255) DEFAULT NULL,
  `order_branch_code` char(6) DEFAULT NULL,
  `order_branch_street` varchar(50) DEFAULT NULL,
  `order_branch_city` varchar(50) DEFAULT NULL,
  `order_branch_zip` varchar(50) DEFAULT NULL,
  `order_branch_phone` varchar(20) DEFAULT NULL,
  `order_branch_email` varchar(100) DEFAULT NULL,
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` datetime NOT NULL,
  `order_shopper_custommer_code` varchar(255) DEFAULT NULL,
  `post_track_trace_code` varchar(255) DEFAULT NULL,
  `is_payu` tinyint(4) NOT NULL DEFAULT '0',
  `payu_session_code` varchar(255) DEFAULT NULL,
  `payu_status_code` varchar(255) DEFAULT NULL,
  `payu_status_message` varchar(255) DEFAULT NULL,
  `payu_error_message` varchar(255) DEFAULT NULL,
  `gift_box_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_id` int(11) NOT NULL,
  `jednotka` char(2) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(5,2) NOT NULL,
  `pocet_na_sklade` decimal(6,2) NOT NULL,
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `tax_code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `units` decimal(6,2) NOT NULL,
  `price_without_tax` decimal(10,2) NOT NULL,
  `price_with_tax` decimal(10,2) NOT NULL,
  `total_price_with_tax` decimal(10,2) NOT NULL,
  `item_change` tinyint(4) DEFAULT '0',
  `total_weight` decimal(6,2) NOT NULL DEFAULT '0.00',
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_item_changes`
--

CREATE TABLE IF NOT EXISTS `order_item_changes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `change_order` bigint(20) NOT NULL,
  `change_item` bigint(20) NOT NULL DEFAULT '0',
  `change_date` datetime DEFAULT NULL,
  `change_units_from` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_units_to` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_type` varchar(10) NOT NULL DEFAULT 'internal',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_states`
--

CREATE TABLE IF NOT EXISTS `order_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `order_state_type_id` int(11) NOT NULL,
  `send_mail` tinyint(4) NOT NULL DEFAULT '0',
  `readonly` tinyint(4) NOT NULL DEFAULT '0',
  `poradi` tinyint(4) NOT NULL,
  `smazano` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_state_type_id` (`order_state_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_data`
--

CREATE TABLE IF NOT EXISTS `order_state_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_state_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email_text` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `order_state_id` (`order_state_id`,`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_types`
--

CREATE TABLE IF NOT EXISTS `order_state_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `owner_data`
--

CREATE TABLE IF NOT EXISTS `owner_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `firma` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `mesto` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `psc` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `stat` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `copyright` varchar(25) COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `tel` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `www` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ga_script` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `owner_data`
--

INSERT INTO `owner_data` (`id`, `default_title`, `default_description`, `default_keywords`, `firma`, `ulice`, `mesto`, `psc`, `stat`, `copyright`, `ic`, `dic`, `tel`, `email`, `www`, `ga_script`) VALUES
(1, 'Orbcomm', '', '', '', '', '', '', NULL, '2014', NULL, NULL, '', 'tom.barborik@dgstudio.cz', NULL, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_category_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `indexpage` tinyint(4) NOT NULL,
  `new_window` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_menu` tinyint(4) DEFAULT '1',
  `direct_to_sublink` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_submenu` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `nav_class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_contactform` tinyint(4) NOT NULL DEFAULT '0',
  `show_child_pages_index` tinyint(4) NOT NULL DEFAULT '0',
  `protected` tinyint(4) NOT NULL DEFAULT '0',
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_photo_detail` tinyint(4) NOT NULL DEFAULT '0',
  `show_visitform` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `page_category_id` (`page_category_id`) USING BTREE,
  KEY `show_in_menu` (`show_in_menu`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_categories`
--

CREATE TABLE IF NOT EXISTS `page_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `admin_zobrazit` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `code` (`code`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_data`
--

CREATE TABLE IF NOT EXISTS `page_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `akce_text` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photos`
--

CREATE TABLE IF NOT EXISTS `page_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photo_data`
--

CREATE TABLE IF NOT EXISTS `page_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_photo_id` (`page_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `typ` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  `predem` tinyint(4) NOT NULL DEFAULT '0',
  `payu` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments_shippings`
--

CREATE TABLE IF NOT EXISTS `payments_shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`,`payment_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payment_data`
--

CREATE TABLE IF NOT EXISTS `payment_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_id` (`payment_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories`
--

CREATE TABLE IF NOT EXISTS `price_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `hodnota` tinyint(4) NOT NULL COMMENT 'pripadna procentni hodnota',
  `zaradit_zakaznika_od` decimal(8,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `price_type_id` (`price_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories_products`
--

CREATE TABLE IF NOT EXISTS `price_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `price_category_id` (`price_category_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_types`
--

CREATE TABLE IF NOT EXISTS `price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `kratky_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `jednotka` varchar(4) COLLATE utf8_czech_ci DEFAULT 'ks',
  `availability` varchar(90) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `rok_vyroby` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(6,3) DEFAULT '0.000',
  `pocet_na_sklade` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `puvodni_cena` decimal(10,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `smazano` tinyint(4) NOT NULL DEFAULT '0',
  `top` tinyint(4) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `import_type` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `original` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `product_expedition_id` int(11) DEFAULT NULL,
  `guarantee` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '0',
  `percentage_discount` decimal(4,2) NOT NULL DEFAULT '0.00',
  `ignore_discount` tinyint(4) NOT NULL DEFAULT '0',
  `new_imported` tinyint(4) NOT NULL DEFAULT '0',
  `imported` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  `gift_threshold_price` decimal(8,0) DEFAULT NULL,
  `product_action_type` tinyint(4) NOT NULL DEFAULT '0',
  `new` tinyint(4) NOT NULL DEFAULT '0',
  `sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `producer_sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `youtube_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stroj_id` int(11) NOT NULL,
  `prefered` tinyint(4) DEFAULT NULL,
  `action` tinyint(4) DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `sec_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manufacturer_id` (`manufacturer_id`) USING BTREE,
  KEY `smazano` (`smazano`) USING BTREE,
  KEY `tax_id` (`tax_id`) USING BTREE,
  KEY `import_type` (`import_type`) USING BTREE,
  KEY `original` (`original`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src_left` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src_right` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `priorita` tinyint(4) NOT NULL DEFAULT '0',
  `special_code` varchar(16) COLLATE utf8_czech_ci NOT NULL COMMENT 'specialni kategorie (novinky, akce, apod.)',
  `class` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `photo_nav_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `template` tinyint(1) NOT NULL DEFAULT '0',
  `show_prod` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_id` int(11) DEFAULT NULL,
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `home_image_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `homepage_text` tinyint(1) NOT NULL DEFAULT '0',
  `demand` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories_products`
--

CREATE TABLE IF NOT EXISTS `product_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `product_category_id` (`product_category_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_data`
--

CREATE TABLE IF NOT EXISTS `product_category_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_full` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_jedno` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_menu` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_paticka` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `uvodni_popis_levy` text COLLATE utf8_czech_ci,
  `uvodni_popis_pravy` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '1',
  `price_from` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `no_show_products` tinyint(4) NOT NULL DEFAULT '0',
  `header` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `product_category_id` (`product_category_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_doplnek` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `odborne_informace` text COLLATE utf8_czech_ci,
  `baleni` text COLLATE utf8_czech_ci,
  `akce_text` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `k_prodeji` tinyint(4) NOT NULL DEFAULT '1',
  `vykon` float unsigned DEFAULT NULL,
  `header` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photos`
--

CREATE TABLE IF NOT EXISTS `product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photo_data`
--

CREATE TABLE IF NOT EXISTS `product_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_photo_id` (`product_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.'),
(3, 'global_admin', 'Global administrator.');

-- --------------------------------------------------------

--
-- Struktura tabulky `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(2, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(2, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(5, 3),
(12, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_seo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_action` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT 'index',
  `param_id1` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `baselang_route_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `internal` tinyint(4) NOT NULL DEFAULT '0',
  `searcheable` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `nazev_seo_old` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nazev_seo` (`nazev_seo`) USING BTREE,
  KEY `page_type_id` (`module_id`) USING BTREE,
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `baselang_route_id` (`baselang_route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `submodule_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_subcode_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `value_subcode_2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poradi` int(11) NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_code` (`module_code`) USING BTREE,
  KEY `submodule_code` (`submodule_code`) USING BTREE,
  KEY `value_code` (`value_code`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `settings`
--

INSERT INTO `settings` (`id`, `module_code`, `submodule_code`, `value_code`, `value_subcode_1`, `value_subcode_2`, `poradi`, `value`, `description`) VALUES
(1, 'page', 'item', 'photo', 't1', 'resize', 1, '600,336,Image::INVERSE', NULL),
(2, 'page', 'item', 'photo', 't1', 'crop', 2, '600,336', NULL),
(3, 'catalog', 'item', 'photo', 't1', 'resize', 1, '82,63,Image::WIDTH', NULL),
(4, 'catalog', 'item', 'photo', 't2', 'resize', 1, '158,120,Image::WIDTH', NULL),
(5, 'catalog', 'item', 'photo', 't1', 'ext', 2, 'png', NULL),
(6, 'catalog', 'item', 'photo', 't2', 'ext', 2, 'png', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `shippings`
--

CREATE TABLE IF NOT EXISTS `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `cenove_hladiny` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_data`
--

CREATE TABLE IF NOT EXISTS `shipping_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_pricelevels`
--

CREATE TABLE IF NOT EXISTS `shipping_pricelevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `level` decimal(8,0) NOT NULL,
  `value` decimal(8,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shoppers`
--

CREATE TABLE IF NOT EXISTS `shoppers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `datum_registrace` datetime DEFAULT NULL,
  `price_category_id` int(11) NOT NULL,
  `order_total` decimal(12,2) NOT NULL,
  `logins` int(10) NOT NULL DEFAULT '0',
  `last_login` int(10) DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `firma` tinyint(4) NOT NULL DEFAULT '0',
  `smazano` tinyint(4) DEFAULT '0',
  `action_first_purchase` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `price_category_id` (`price_category_id`,`smazano`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shopper_branches`
--

CREATE TABLE IF NOT EXISTS `shopper_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) COLLATE utf8_czech_ci NOT NULL,
  `shopper_id` int(11) NOT NULL,
  `smazano` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content`
--

CREATE TABLE IF NOT EXISTS `static_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content_data`
--

CREATE TABLE IF NOT EXISTS `static_content_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `static_content_id` int(11) NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `static_page_id` (`static_content_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `taxes`
--

CREATE TABLE IF NOT EXISTS `taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `hodnota` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `taxes`
--

INSERT INTO `taxes` (`id`, `nazev`, `code`, `hodnota`) VALUES
(1, 'žádná', 'no_vat', 0),
(2, 'nizší', 'lower_vat', 15),
(3, 'vyšší', 'higher_vat', 21);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`) USING BTREE,
  UNIQUE KEY `uniq_email` (`email`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `created_by`) VALUES
(5, 'info.vzak@gmail.com', 'hana', 'e67fcbdb1f94a7645d31ff55cbdcee5925b5ef1381e5b6f2e4', 305, 1391531214, 5),
(7, 'info@dgstudio.cz', 'dgstudio', '69142eb7316a41f4f72bba0eb52aeb748be1ed742669f98907', 397, 1418562605, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_admin_prefernces`
--

CREATE TABLE IF NOT EXISTS `user_admin_prefernces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admin_preference_id` int(11) DEFAULT NULL,
  `value` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime_start` datetime NOT NULL,
  `datetime_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_rights`
--

CREATE TABLE IF NOT EXISTS `user_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `permission` tinyint(11) NOT NULL DEFAULT '0',
  `readonly` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `module_name` (`module_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`) USING BTREE,
  KEY `fk_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `vouchers`
--

CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `discount_value` decimal(2,0) NOT NULL COMMENT 'sleva % z D0',
  `one_off` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'jednorazovy kupon',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `used` int(11) NOT NULL DEFAULT '0' COMMENT 'pocet pouziti',
  `lifetime` date DEFAULT NULL,
  `shopper_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
